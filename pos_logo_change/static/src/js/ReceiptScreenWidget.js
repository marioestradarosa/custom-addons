// ReceiptScreenWidget
odoo.define('pos_logo_change.ReceiptScreenWidget', function(require) {
	"use strict";

	const OrderReceipt = require('point_of_sale.OrderReceipt');
    const Registries = require('point_of_sale.Registries');

    const ReceiptScreenWidget = OrderReceipt => 
        class extends OrderReceipt {
        	constructor() {
				super(...arguments);
				console.log(this,"-----this")
			}
			
            get receiptBarcode(){
            	console.log(this,"ReceiptScreen---------",$("#barcode_print"))
				var order = this.env.pos.get_order();
				$("#barcode_print").barcode(
					order.barcode, // Value barcode (dependent on the type of barcode)
					"code128" // type (string)
				);
			return true
            }
        
    };

    Registries.Component.extend(OrderReceipt, ReceiptScreenWidget);

    return OrderReceipt;
});