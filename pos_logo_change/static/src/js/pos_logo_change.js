// pos_logo_change js
//console.log("custom callleddddddddddddddddddddd")
odoo.define('pos_logo_change.pos',function(require){
	"use strict";
	var models = require('point_of_sale.models');
	const chrome=require('point_of_sale.Chrome');
    const screens = require('point_of_sale.ReceiptScreen');
    const core = require('web.core');
    const gui = require('point_of_sale.Gui');
    var QWeb = core.qweb;

    var _t = core._t;



    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            var company_model = _.find(this.models, function(model){ return model.model === 'res.company'; });
            company_model.fields.push('logo','street','city','state_id');
            // Push Other fields of res.company so we can access those fields in POS... 
            return _super_posmodel.initialize.call(this, session, attributes);
        },
        
    });
    

	var OrderSuper = models.Order;
	var posorder_super = models.Order.prototype;
	models.Order = models.Order.extend({
		initialize: function() {
			console.log(this,"-----this2")
			this.barcode = this.barcode || "";
			console.log(this.barcode,"-----barcode")
			this.set_barcode();
			posorder_super.initialize.apply(this,arguments);
		},

		set_barcode: function(){
			var self = this;	
			var temp = Math.floor(100000000000+ Math.random() * 9000000000000)
			self.barcode =  temp.toString();
		},

		export_as_JSON: function() {
			var self = this;
			var loaded = OrderSuper.prototype.export_as_JSON.call(this);
			loaded.barcode = self.barcode;
			return loaded;
		},

	});

});;
